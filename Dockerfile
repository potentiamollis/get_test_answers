FROM ubuntu

LABEL developer="potentiamollis@gmail.ru"

COPY . /app

WORKDIR /app
# Set the Chrome repo.
RUN mv chromedriver /usr/local/bin/chromedriver

RUN apt-get update && apt-get upgrade && apt-get install -y python3

RUN apt-get install -y pip 

RUN pip install selenium

CMD ["python3", "./get_testing_answers_eng.py"]
