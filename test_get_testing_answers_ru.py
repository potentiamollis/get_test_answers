import random
import time
import json
import pytest

from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

#testsdfsdf
link = "https://tests.if.ua/"

@pytest.fixture
def driver():
    print("/ninit fixture for driver")
    driver = webdriver.Chrome()
    return driver



class TestGetAnswers():
    
    def test_abs1(self):
        failed = 0
        for k in range(1):  # тут вводить количество циклов, сколько раз должен отработать автотест
            try:
                driver = webdriver.Chrome()

                driver.get("https://tests.if.ua/")


                textarea_facultet = WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.XPATH, '//h2[text() = "Медичний факультет"]'))
                    )
                #driver.find_element_by_xpath('//h2[text() = "Медичний факультет"]')
                textarea_facultet.click()


                textarea_course = WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.XPATH, '//h2[text() = "5 Курс"]'))
                    )
                #driver.find_element_by_xpath('//h2[text() = "5 Курс"]')   #
                textarea_course.click()

                time.sleep(5)

            
                student_category = WebDriverWait(driver, 10).until(
                        EC.element_to_be_clickable((By.ID, 'student-category'))
                    )
                #driver.find_element_by_id("student-category")
                student_category.click()


                subject = driver.find_element_by_xpath('//div[@id="student-category"]/div[1]//ul/li/div[11]/a/span').text
                # print([my_elem.text for my_elem in WebDriverWait(driver, 20).until(EC.visibility_of_all_elements_located(
                #    (By.XPATH, '//div[@id="student-category"]/div[1]//ul/li/div[3]/a/span')))])

                textarea_subject = WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.XPATH, '//div[@id="student-category"]/div[1]//ul/li/div[11]'))
                    )
                #driver.find_element_by_xpath('//div[@id="student-category"]/div[1]//ul/li/div[11]') # в div[3] указывать числа от 3
                textarea_subject.click()


                student_category_1 = WebDriverWait(driver, 6).until(
                        EC.element_to_be_clickable((By.XPATH, '//div[@id="student-category"]/div[2]'))
                    )
                #driver.find_element_by_xpath('//div[@id="student-category"]/div[2]')   
                student_category_1.click()

                textarea_theme = WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.XPATH, '//div[@id="student-category"]/div[2]//ul/li/div[4]/a'))
                    )
                #driver.find_element_by_xpath('//div[@id="student-category"]/div[2]//ul/li/div[4]/a') # в div[3] указывать числа от 3
                textarea_theme.click()


                surname = WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.CSS_SELECTOR, '#student-info > form > div:nth-child(1) > input'))
                    )
                #driver.find_element_by_css_selector("#student-info > form > div:nth-child(1) > input")
                surname.send_keys(f"vdf")

                time.sleep(5)


                name = driver.find_element_by_css_selector("#student-info > form > div:nth-child(2) > input")
                name.send_keys("ghmhmf")
                group = driver.find_element_by_css_selector("#student-info > form > div:nth-child(3) > input")

                group.send_keys(f"{k * random.randrange(1, 5)}")

                start_test = driver.find_element_by_css_selector("#student-info > form+button")
                start_test.click()



                end_test = WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.TAG_NAME, 'button'))
                    )
                #driver.find_element_by_tag_name("button")
                end_test.click()


                confirm = driver.switch_to.alert
                confirm.accept()


                driver.get("http://tests.if.ua/api/Stat")

                feedback = driver.find_element_by_tag_name("body").text

                data = json.loads(feedback)

                for question_count in range(len(data['questions'])):
                    for answer_count in range(len(data['stat'])):
                        if data["stat"][question_count]["true_answer_id"] == data["questions"][question_count]["answers"][answer_count]["id"]:
                            question = str(1 + int(question_count)) + ')' + data["questions"][question_count]["text"] + '\n'
                            answer = 'Answer:' + data["questions"][question_count]["answers"][answer_count]["text"] + '\n' + '\n'
                            with open(f'{subject}.txt', 'a') as file:
                                file.write(question)
                            with open(f'{subject}.txt', 'a') as file:
                                file.write(answer)
                            break
            
        
            except Exception:
                print("Exception was catched!")
                failed += 1
                continue

            finally:
                print(k)
                driver.quit()

        print(f"Из {k + 1} тестов успешно выполнилось {k + 1 - failed} тестов.")
        assert 2 * 2 == 4, f"kssk {k}"

class TestSimp():

    def test_abs2():
        try:
                driver = webdriver.Chrome()

                driver.get("https://tests.if.ua/")

                textarea_facultet = WebDriverWait(driver, 5).until(
                        EC.element_to_be_clickable((By.XPATH, '//h2[text() = "Медичний факультет"]'))
                    )
                #driver.find_element_by_xpath('//h2[text() = "Медичний факультет"]')
                textarea_facultet.click()

                assert 2 + 2 == 4, "There is some error"
                with pytest.raises(NoSuchElementException):
                    driver.find_element_by_xpath("dsdd")
                    pytest.fail("The element is existing")
        finally:
                driver.quit()
